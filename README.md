# САБД - Lab 4
## О задании
Name: Создание маршрутизатора для клиент-серверного приложения, работающего через Gateway посредством библиотеки Netflix Zuul

Author: Andrianov Artemii

Group: #5140904/30202

## Цель
Необходимо написать две программы (клиент и сервер) с использованием библиотеки Spring Boot. 
Клиент должен вызывать API сервера, используя прокси Zuul. 
Также в рамках лабораторной работы необходимо написать пользовательский фильтр Zuul.

## Решение
### About custom-zull
Так как для Golang нет прокси сервера Zull - был написан собственных обратный прокси с поддержкой before и after обработчиков.

Исходный код доступен по ссылке: https://gitlab.com/sbpstu-2023-bdas/sbpstu-2023-bdas-custom-zuul
### Build
For build service use `make build`. `.output/service` will be created.

### Params
Flags:

| name   | default | description                  |
|--------|--------|------------------------------|
| config | config | path to YAML configuration   |

For run service user:
```bash 
    ./output/service -config=<your-path-to-config>
```
### Configuration
YAML Configuration example: [config.yaml](configs/config.yaml). 
