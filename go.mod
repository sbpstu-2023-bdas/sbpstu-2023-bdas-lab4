module gitlab.com/sbpstu-2023-bdas/sbpstu-2023-bdas-lab4

go 1.21.4

require (
	github.com/gorilla/mux v1.8.1
	gitlab.com/sbpstu-2023-bdas/sbpstu-2023-bdas-custom-zuul v0.0.3
	go.uber.org/zap v1.26.0
	gopkg.in/yaml.v3 v3.0.1
)

require go.uber.org/multierr v1.10.0 // indirect
