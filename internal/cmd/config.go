package cmd

import "gitlab.com/sbpstu-2023-bdas/sbpstu-2023-bdas-custom-zuul/middleware"

type Config struct {
	Proxy      *ProxyServerConfig `yaml:"proxy"`
	WebServers []WebServerConfig  `yaml:"web-servers"`
}

type ProxyServerConfig struct {
	Addr    string              `yaml:"addr"`
	Targets []middleware.Target `yaml:"targets"`
}

type WebServerConfig struct {
	Addr               string `yaml:"addr"`
	Enable             bool   `yaml:"enable"`
	EndPoint           string `yaml:"end-point"`
	ResponseMessage    string `yaml:"response-message"`
	ResponseStatusCode int    `yaml:"response-status-code"`
}
