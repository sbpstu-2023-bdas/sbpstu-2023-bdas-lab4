package cmd

import (
	"github.com/gorilla/mux"
	"gitlab.com/sbpstu-2023-bdas/sbpstu-2023-bdas-custom-zuul/middleware"
	"go.uber.org/zap"
	"log"
	"net/http"
)

type WebServer struct {
	logger *zap.Logger
	server *http.Server
}

func (w *WebServer) Serve() {
	w.logger.Info("start web server",
		zap.String("addr", w.server.Addr))
	go w.server.ListenAndServe()
}

func (w *WebServer) Stop() {
	defer w.logger.Sync()
	w.logger.Info("stop web server",
		zap.String("addr", w.server.Addr))
	w.server.Close()
}

type App struct {
	logger      *zap.Logger
	proxyServer http.Server
	webServers  []WebServer
}

func NewApp(config *Config, logger *zap.Logger) *App {
	webServers := make([]WebServer, 0)
	for _, server := range config.WebServers {
		if server.Enable {
			webServers = append(webServers, newWebServer(server))
		} else {
			logger.Info("server disabled", zap.String("addr", server.Addr))
		}
	}
	proxyServer := newProxyServer(config.Proxy)

	log.Println(webServers)

	return &App{
		logger:      logger,
		proxyServer: proxyServer,
		webServers:  webServers,
	}
}

func (a *App) Run() {
	a.logger.Info("start web servers", zap.Int("count", len(a.webServers)))
	for _, server := range a.webServers {
		go func(s WebServer) {
			s.Serve()
		}(server)
	}
	a.logger.Info("start proxy server",
		zap.String("addr", a.proxyServer.Addr))
	a.proxyServer.ListenAndServe()
}

func (a *App) Stop() {
	for _, server := range a.webServers {
		server.Stop()
	}
	a.logger.Info("stop proxy server",
		zap.String("addr", a.proxyServer.Addr))
	a.proxyServer.ListenAndServe()
}

// newWebServer create remote web server with routers.
func newWebServer(config WebServerConfig) WebServer {
	logger, _ := zap.NewProduction()
	r := mux.NewRouter()
	logger.Info("create new server with",
		zap.String("addr", config.Addr),
		zap.String("endpoint", config.EndPoint),
		zap.String("message", config.ResponseMessage),
		zap.Int("code", config.ResponseStatusCode),
	)
	r.HandleFunc(config.EndPoint, func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(config.ResponseStatusCode)
		w.Write([]byte(config.ResponseMessage))
		logger.Info("get new request from",
			zap.String("server", config.Addr),
			zap.String("url", r.URL.String()),
		)
	})

	return WebServer{
		server: &http.Server{
			Addr:    config.Addr,
			Handler: r,
		},
		logger: logger,
	}
}

// newProxyServer create reverse proxy server.
func newProxyServer(config *ProxyServerConfig) http.Server {
	logger, _ := zap.NewProduction()
	defer logger.Sync()
	proxy := middleware.NewProxy(config.Targets)

	setHeaderFunc := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Before", "Some test value")
		logger.Info("message in setHeaderFunc", zap.String("url", r.URL.String()))
	}
	appendBodyFunc := func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("hello from appendBodyFunc\n"))
		logger.Info("message in appendBodyFunc", zap.String("url", r.URL.String()))
	}

	before := middleware.NewAccompanimentBefore([]middleware.AccompanimentFunc{setHeaderFunc, appendBodyFunc})

	writeSomeLogInMessage := func(w http.ResponseWriter, r *http.Request) {
		logger.Info("message in writeSomeLogInMessage", zap.String("url", r.URL.String()))
	}

	after := middleware.NewAccompanimentAfter([]middleware.AccompanimentFunc{writeSomeLogInMessage})

	r := mux.NewRouter()

	r.PathPrefix("/").Handler(before(after(proxy(http.DefaultServeMux))))

	return http.Server{
		Addr:    config.Addr,
		Handler: r,
	}
}
