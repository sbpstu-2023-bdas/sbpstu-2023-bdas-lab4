package main

import (
	"flag"
	"gitlab.com/sbpstu-2023-bdas/sbpstu-2023-bdas-lab4/internal/cmd"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
	"log"
	"os"
)

func main() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()
	pathToConfig := flag.String("config", "configs/config.yaml", "path to YAML config")
	config, err := getConfig(*pathToConfig)
	if err != nil {
		logger.Panic("error while parse config",
			zap.Error(err))
	}
	log.Println(config.WebServers)
	app := cmd.NewApp(config, logger)
	app.Run()
	defer app.Stop()
}

func getConfig(pathToConfig string) (*cmd.Config, error) {
	file, err := os.Open(pathToConfig)
	if err != nil {
		return nil, err
	}
	var config cmd.Config
	if err := yaml.NewDecoder(file).Decode(&config); err != nil {
		return nil, err
	}
	return &config, nil

}
